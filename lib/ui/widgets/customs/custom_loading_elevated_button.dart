import 'package:flutter/material.dart';

class CustomLoadingElevatedButton extends StatelessWidget {
  final bool isLoading;
  final Widget child;
  final void Function()? onPressed;

  const CustomLoadingElevatedButton({
    Key? key,
    this.isLoading = false,
    required this.child,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: isLoading
          ? const SizedBox(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(color: Colors.white),
            )
          : child,
      onPressed: onPressed,
    );
  }
}
