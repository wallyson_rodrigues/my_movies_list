import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title/title_model.dart';

import 'package:my_movies_list/ui/pages/title/title_details_page.dart';
import 'package:my_movies_list/ui/widgets/title/custom_carousel.dart';
import 'package:my_movies_list/ui/widgets/title/title_thumbnail.dart';

class TitleCarousel extends StatelessWidget {
  final List<TitleModel> titles;
  final String label;

  const TitleCarousel({
    Key? key,
    required this.titles,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCarousel(
      label: label,
      children: titles.map((e) => _buildTitleCard(context: context, title: e)).toList(),
    );
  }

  Widget _buildTitleCard({required TitleModel title, required BuildContext context}) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, TitleDetailsPage.name, arguments: {'id': title.id, 'is_tv_show': title.isTvShow}),
      child: TitleThumbnail(
        width: 115.0,
        height: 190.0,
        titleName: title.name,
        urlThumbnail: title.posterUrl,
      ),
    );
  }
}
