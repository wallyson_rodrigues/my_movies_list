import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/home/home_cubit.dart';
import 'package:my_movies_list/ui/widgets/title/title_carousel.dart';

class MainTabPage extends StatelessWidget {
  const MainTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                _buildUpcomingMovieList(context, 'Filmes recentes'),
                _buildPopularMovieList(context, 'Séries populares'),
                _buildPopularTvList(context, 'Filmes populares'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPopularMovieList(BuildContext context, String label) {
    context.read<HomeCubit>().getTitleList(
          TitleFilter(
            typeList: TypeList.popularMovieList,
            labelList: label,
          ),
        );

    return _buildList(label);
  }

  Widget _buildPopularTvList(BuildContext context, String label) {
    context.read<HomeCubit>().getTitleList(
          TitleFilter(
            typeList: TypeList.popularTvList,
            labelList: label,
          ),
        );

    return _buildList(label);
  }

  Widget _buildUpcomingMovieList(BuildContext context, String label) {
    context.read<HomeCubit>().getTitleList(
          TitleFilter(
            typeList: TypeList.upcomingMovie,
            labelList: label,
          ),
        );

    return _buildList(label);
  }

  Widget _buildList(String label) {
    return BlocBuilder<HomeCubit, HomeState>(
      buildWhen: (previous, current) {
        if (current is SuccessHomeState) {
          return current.labelList == label;
        }

        return true;
      },
      builder: (context, state) {
        if (state is ProcessingHomeState) {
          return const Center(child: CircularProgressIndicator());
        }

        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: TitleCarousel(
            label: label,
            titles: (state as SuccessHomeState).titles,
          ),
        );
      },
    );
  }
}
