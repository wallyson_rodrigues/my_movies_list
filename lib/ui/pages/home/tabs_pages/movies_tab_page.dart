import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/home/home_cubit.dart';
import 'package:my_movies_list/data/models/genre/genre_model.dart';
import 'package:my_movies_list/ui/widgets/title/title_carousel.dart';

class MoviesTabPage extends StatelessWidget {
  MoviesTabPage({Key? key}) : super(key: key);

  final genres = <GenreModel>[
    GenreModel(id: 16, name: 'Animação'),
    GenreModel(id: 12, name: 'Aventura'),
    GenreModel(id: 27, name: 'Terror'),
  ];

  @override
  Widget build(BuildContext context) {
    context.read<HomeCubit>().getTitleList(
          TitleFilter(
            genres: genres,
            typeList: TypeList.movieList,
          ),
        );

    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: genres.map((e) {
                return BlocBuilder<HomeCubit, HomeState>(
                  buildWhen: (previous, current) {
                    if (current is SuccessHomeState) {
                      return current.labelList == e.name;
                    }

                    return true;
                  },
                  builder: (context, state) {
                    if (state is ProcessingHomeState) {
                      return const Center(child: CircularProgressIndicator());
                    }

                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: TitleCarousel(
                        label: e.name,
                        titles: (state as SuccessHomeState).titles,
                      ),
                    );
                  },
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
