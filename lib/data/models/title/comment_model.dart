class CommentModel {
  int id;
  String text;
  DateTime date;

  CommentModel({required this.id, required this.text, required this.date});

  factory CommentModel.fromJson(Map<String, dynamic> json) {
    return CommentModel(
      id: json['id'],
      text: json['text'] ?? '',
      date: DateTime.parse(json['date']),
    );
  }
}
