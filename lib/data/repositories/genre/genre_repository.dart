import 'package:my_movies_list/data/models/genre/genre_model.dart';
import 'package:my_movies_list/data/repositories/genre/genre_repository_inteface.dart';
import 'package:my_movies_list/data/services/http_service.dart';
import 'package:my_movies_list/shared/strings.dart';

class GenreRepository implements GenreRepositoryInterface {
  final HttpService _service;
  final String _baseUrl = Strings.movieApiUrl;

  GenreRepository(this._service);

  @override
  Future<List<GenreModel>> getGenreList() async {
    String uri = '$_baseUrl/movies/';
    var response = await _service.getRequest(uri);

    if (response.sucess) {
      List<dynamic> data = response.content!['data'];
      return List<GenreModel>.from(data.map((json) => GenreModel.fromJson(json)));
    }

    return [];
  }
}
