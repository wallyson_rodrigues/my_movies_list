import 'package:my_movies_list/data/models/genre/genre_model.dart';

abstract class GenreRepositoryInterface {
  Future<List<GenreModel>> getGenreList();
}
