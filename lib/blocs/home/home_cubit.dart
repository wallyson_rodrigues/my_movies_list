import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:my_movies_list/data/models/genre/genre_model.dart';
import 'package:my_movies_list/data/models/title/title_model.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';

abstract class HomeState {}

class ProcessingHomeState implements HomeState {}

class SuccessHomeState implements HomeState {
  final List<TitleModel> titles;
  final String? labelList;

  SuccessHomeState(this.titles, {this.labelList});
}

enum TypeList { movieList, tvList, popularMovieList, popularTvList, upcomingMovie }

class TitleFilter {
  List<GenreModel>? genres;
  String? labelList;
  TypeList typeList;

  TitleFilter({this.genres, this.labelList, required this.typeList});
}

class HomeCubit extends Cubit<HomeState> {
  final TitleRepositoryInterface _repository;

  HomeCubit(this._repository) : super(ProcessingHomeState());

  Future<void> getTitleList(TitleFilter filter) async {
    switch (filter.typeList) {
      case TypeList.movieList:
        filter.genres!.forEach((e) async {
          emit(ProcessingHomeState());
          var response = await _repository.getMovieList(params: {'genre': e.id});
          emit(SuccessHomeState(response, labelList: e.name));
        });
        break;

      case TypeList.tvList:
        filter.genres!.forEach((e) async {
          emit(ProcessingHomeState());
          var response = await _repository.getTvList(params: {'genre': e.id});
          emit(SuccessHomeState(response, labelList: e.name));
        });
        break;

      case TypeList.popularMovieList:
        emit(ProcessingHomeState());
        var response = await _repository.getPopularMovieList();
        emit(SuccessHomeState(response, labelList: filter.labelList));
        break;

      case TypeList.popularTvList:
        emit(ProcessingHomeState());
        var response = await _repository.getPopularTvList();
        emit(SuccessHomeState(response, labelList: filter.labelList));
        break;

      case TypeList.upcomingMovie:
        emit(ProcessingHomeState());
        var response = await _repository.getUpcomingMovieList();
        emit(SuccessHomeState(response, labelList: filter.labelList));
        break;

      default:
        break;
    }
  }
}
