import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title/title_rated_model.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';

abstract class RatedTitlesListState {}

class ProcessingRatedTitleListState implements RatedTitlesListState {}

class SuccessRatedTitleListState implements RatedTitlesListState {
  List<TitleRatedModel> ratings;

  SuccessRatedTitleListState(this.ratings);
}

class FailRatedTitleListState implements RatedTitlesListState {}

class RatedTitlesListCubit extends Cubit<RatedTitlesListState> {
  final TitleRepositoryInterface _repository;

  RatedTitlesListCubit(this._repository) : super(ProcessingRatedTitleListState());

  Future<void> getUserRatedTitleList(String? userId) async {
    emit(ProcessingRatedTitleListState());

    try {
      var response = await _repository.getUserRatedTitleList(userId);
      emit(SuccessRatedTitleListState(response));
    } catch (e) {
      emit(FailRatedTitleListState());
    }
  }
}
