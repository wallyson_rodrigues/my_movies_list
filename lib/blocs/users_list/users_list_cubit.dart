import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:my_movies_list/data/models/user/user_model.dart';
import 'package:my_movies_list/data/repositories/user/user_repository_interface.dart';

abstract class UsersListState {}

class ProcessingUserListState implements UsersListState {}

class SuccessUserListState implements UsersListState {
  List<UserModel> users;

  SuccessUserListState(this.users);
}

class FailUserListState implements UsersListState {}

class UsersListCubit extends Cubit<UsersListState> {
  final UserRepositoryInterface _repository;

  UsersListCubit(this._repository) : super(ProcessingUserListState());

  Future<void> listUsers() async {
    emit(ProcessingUserListState());

    try {
      var response = await _repository.listUsers();
      emit(SuccessUserListState(response));
    } catch (e) {
      emit(FailUserListState());
    }
  }
}
