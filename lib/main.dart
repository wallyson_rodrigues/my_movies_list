import 'package:flutter/material.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home/home_page.dart';
import 'package:my_movies_list/ui/pages/login/login_page.dart';
import 'package:my_movies_list/ui/pages/title/rated_title_list_page.dart';
import 'package:my_movies_list/ui/pages/register_account/register_account_page.dart';
import 'package:my_movies_list/ui/pages/search/search_page.dart';
import 'package:my_movies_list/ui/pages/splash/splash_page.dart';
import 'package:my_movies_list/ui/pages/title/title_details_page.dart';
import 'package:my_movies_list/ui/pages/user/user_list_page.dart';

void main() {
  setUpLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      initialRoute: SplashPage.name,
      routes: {
        LoginPage.name: (_) => const LoginPage(),
        HomePage.name: (_) => const HomePage(),
        SearchPage.name: (_) => const SearchPage(),
        TitleDetailsPage.name: (_) => const TitleDetailsPage(),
        RegisterAccountPage.name: (_) => const RegisterAccountPage(),
        RatedTitleListPage.name: (_) => const RatedTitleListPage(),
        UserListPage.name: (_) => const UserListPage(),
        SplashPage.name: (_) => const SplashPage(),
      },
    );
  }
}
